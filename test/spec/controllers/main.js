'use strict';

describe('Controller: AmWbCarouselCtrl', function () {

  // load the controller's module
  beforeEach(module('amWbCarousel'));

  var scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(angular.isDefined(scope)).toBe(true);
  });
});
