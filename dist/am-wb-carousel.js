/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * @ngdoc overview
 * @name amWbCarousel
 * @description # amWbCarousel
 * 
 * Main module of the application.
 */
angular
    .module('amWbCarousel', [
        'ngMaterialWeburger',//
        'ngSanitize'
    ]);

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('amWbCarousel')
/**
 * 
 */
.config(function(ngMdIconServiceProvider) {
	ngMdIconServiceProvider
	// Move actions
	.addShapes({
		'wb-widget-carousel': ngMdIconServiceProvider.getShape('burst_mode'),
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbCarousel')//

/**
 * 
 * Manage an slider
 */
.controller('AmWbCarouselCtrl', function($scope, $timeout, $interval) {
	var wbModel = $scope.wbModel;
	var timestamp = +new Date();
	var currentIndex = 0;

	/**
	 * Show slide with idx index
	 * 
	 */
	function showSlide(idx) {
		if (idx === undefined) {
			return;
		}
		if (idx >= wbModel.slides.length) {
			idx = 0;
		}
		if (idx < 0) {
			idx = wbModel.slides.length - 1;
		}
		currentIndex = idx;
		$scope.currentIndex = idx;
		$scope.slides = wbModel.slides;
		$scope.slide = wbModel.slides[currentIndex];
	}

	/**
	 * Show next slied
	 */
	function showNext() {
		showSlide(currentIndex + 1);
	}

	/**
	 * Show prev. slied
	 */
	function showPrev() {
		showSlide(currentIndex - 1);
	}

	/**
	 * Skip n slied
	 */
	function plusSlides(n) {
		showSlide(currentIndex + n);
	}

	/**
	 * Push new empty slide
	 */
	function addSlide() {
		wbModel.slides.push({
			image : 'http://media.istockphoto.com/photos/plant-growing-picture-id510222832',
			caption : '<h1>Sample Caption</h1>'
		});
	}
	
	/**
	 * Remove slide in specified index
	 */
	function removeSlide(index) {
		wbModel.slides.splice(index, 1);
		showSlide(index - 1 < 0 ? 0 : index - 1);
	}

	// Slied function in scope
	$scope.plusSlides = plusSlides;
	$scope.showSlide = showSlide;
	$scope.add = addSlide;
	$scope.remove = removeSlide;

	showSlide(0);

	$scope.extraActions = [ {
		title : 'Add slide',
		icon : 'add',
		action : addSlide
	}, {
		title : 'Remove slide',
		icon : 'remove',
		action : function(){
			removeSlide(currentIndex)
		}
	} ];

	$scope.$watch('wbModel.slides', function() {
		showSlide(0);
	});

	// TODO: check if is required in current settins
	var autoplayPromise = $interval(function() {
		if (wbModel.autoplay) {
			showNext();
		}
	}, wbModel.autoplayTime);

	$scope.$watch('wbModel.autoplayTime', function() {
		$interval.cancel(autoplayPromise);
		autoplayPromise = $interval(function() {
			if (wbModel.autoplay) {
				showNext();
			}
		}, wbModel.autoplayTime);
	});
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbCarousel')

/**
 * Load settings
 */
.run(function($settings) {
	$settings.newPage({
		type: 'carousel-setting',
		label : 'Carousel Settings',
		description : 'Set attributes of carousel',
		controller: 'AmWbCarouselCtrl',
		templateUrl : 'views/am-wb-carousel-settings/carousel.html'
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbCarousel')
/**
 * load widgets
 */
.run(function($widget) {
	var slides = [{
        title : 'slide 1',
        value : 0,
        image : 'http://media.istockphoto.com/photos/starry-night-picture-id519760984',
        caption : '<h1>Caption Text for Slide 01</h1>',
        style : {}
    }, {
        title : 'slide 2',
        value : 1,
        image : 'http://media.istockphoto.com/photos/autumn-tree-and-sun-during-sunset-fall-in-park-picture-id520757917',
        caption : '<h1>Caption Text for Slide 02</h1>',
        style : {}
    }, {
        title : 'slide 3',
        value : 2,
        image : 'http://media.istockphoto.com/photos/plant-growing-picture-id510222832',
        caption : '<h1>Caption Text for Slide 03</h1>',
        style : {}
    }];
	
	$widget.newWidget({
	    type: 'AmWbCarouselWidget',
	    templateUrl : 'views/am-wb-carousel-widgets/simple.html',
	    label : 'Carousel',
	    description : 'List of slides as carousel.',
	    icon : 'wb-widget-carousel',
	    help : 'https://gitlab.com/weburger/am-wb-carousel/wikis/home',
	    controller: 'AmWbCarouselCtrl',
	    setting:['carousel-setting'],
	    data:{
	    	name: 'Slider',
	    	slides: slides
	    }
	});

	$widget.newWidget({
		type: 'AmWbCarouselNavigator',
		templateUrl : 'views/am-wb-carousel-widgets/navigator.html',
		label : 'Carousel navigator',
		description : 'List of slides as carousel with a navigator on top.',
		icon : 'wb-widget-carousel',
		help : 'https://gitlab.com/weburger/am-wb-carousel/wikis/home',
		controller: 'AmWbCarouselCtrl',
		setting:[
			'carousel-setting'
		],
		data:{
			name: 'Widget slider',
			slides: slides
		}
	});
});

angular.module('amWbCarousel').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-carousel-settings/carousel.html',
    " <md-list class=wb-setting-panel>  <md-subheader class=md-no-sticky>General <md-button class=md-icon-button ng-click=add()> <wb-icon>add</wb-icon> </md-button> </md-subheader> <wb-ui-setting-on-off-switch title=Autoplay? icon=play_circle_outline value=wbModel.autoplay> </wb-ui-setting-on-off-switch> <wb-ui-setting-number ng-show=wbModel.autoplay title=\"Time (millisecond)\" icon=schedule value=wbModel.autoplayTime> </wb-ui-setting-number>  <md-subheader layout=row ng-repeat-start=\"slide in wbModel.slides\" class=md-no-sticky> <span>Settings for slide {{$index + 1}}</span> <span flex></span> <md-button class=md-icon-button ng-click=remove($index)> <wb-icon>delete</wb-icon> </md-button> </md-subheader> <wb-ui-setting-image value=slide.image title=Image aria-label=Image> </wb-ui-setting-image> <md-input-container ng-repeat-end class=\"md-icon-float md-block\"> <label>Caption</label> <input ng-model=slide.caption aria-label=Caption> </md-input-container> </md-list>"
  );


  $templateCache.put('views/am-wb-carousel-widgets/navigator.html',
    "<div class=carousel-navigator-slideshow-container> <div> <img class=carousel-navigator-thumnail data-ng-repeat=\"slide in slides\" data-ng-src={{slide.image}} data-ng-click=showSlide($index)> <md-button class=carousel-navigator-thumnail ng-show=wbEditable ng-click=add()> <wb-icon>add</wb-icon> </md-button> </div> <div class=carousel-navigator-fade> <div class=carousel-navigator-numbertext>{{currentIndex+1}} / {{slides.length}}</div> <div ng-show=wbEditable class=carousel-navigator-actions> <md-button class=md-icon-button ng-click=remove(currentIndex)> <wb-icon>delete</wb-icon> </md-button> </div> <img class=carousel-navigator-img src={{slide.image}} alt={{slide.image}}> <div class=carousel-navigator-text ng-hide=isSelected() ng-bind-html=\"slide.caption | wbunsafe\"> </div> <div class=carousel-navigator-text ui-tinymce=\"{\n" +
    "\t\t\t\tselector : 'div.tinymce',\n" +
    "\t\t\t\ttheme : 'inlite',\n" +
    "\t\t\t\tplugins : 'directionality contextmenu table link paste image imagetools hr textpattern autolink textcolor colorpicker ',\n" +
    "\t\t\t\tinsert_toolbar : 'quickimage quicktable',\n" +
    "\t\t\t\tselection_toolbar : 'bold italic | quicklink h1 h2 h3 blockquote | ltr rtl | forecolor',\n" +
    "\t\t\t\tinsert_button_items: 'image link | inserttable | hr',\n" +
    "\t\t\t\tinline : true,\n" +
    "\t\t\t\tpaste_data_images : true,\n" +
    "\t\t\t\tbranding: false,\n" +
    "\t\t\t\timagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions'\n" +
    "\t\t\t}\" ng-model=slide.caption ng-show=isSelected() flex> </div> </div> </div>"
  );


  $templateCache.put('views/am-wb-carousel-widgets/simple.html',
    "<div class=carousel-simple-slideshow-container> <div class=carousel-simple-fade> <div class=carousel-simple-numbertext>{{currentIndex+1}} / {{slides.length}}</div> <img class=carousel-simple-img src={{slide.image}} alt={{slide.image}}> <div class=carousel-simple-text ng-hide=isSelected() ng-bind-html=\"slide.caption | wbunsafe\"> </div> <div class=carousel-simple-text ui-tinymce=\"{\n" +
    "\t\t\t\tselector : 'div.tinymce',\n" +
    "\t\t\t\ttheme : 'inlite',\n" +
    "\t\t\t\tplugins : 'directionality contextmenu table link paste image imagetools hr textpattern autolink textcolor colorpicker ',\n" +
    "\t\t\t\tinsert_toolbar : 'quickimage quicktable',\n" +
    "\t\t\t\tselection_toolbar : 'bold italic | quicklink h1 h2 h3 blockquote | ltr rtl | forecolor',\n" +
    "\t\t\t\tinsert_button_items: 'image link | inserttable | hr',\n" +
    "\t\t\t\tinline : true,\n" +
    "\t\t\t\tpaste_data_images : true,\n" +
    "\t\t\t\tbranding: false,\n" +
    "\t\t\t\timagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions'\n" +
    "\t\t\t}\" ng-model=slide.caption ng-show=isSelected() flex> </div> <div class=carousel-simple-dots> <span data-ng-repeat=\"slide in wbModel.slides\" data-ng-class=\"{\n" +
    "\t\t\t\t\t'carousel-simple-dot': true, \n" +
    "\t\t\t\t\t'carousel-simple-active': currentIndex === $index \n" +
    "\t\t\t\t}\" data-ng-click=showSlide($index)> </span> </div> </div> <a class=carousel-simple-prev data-ng-click=plusSlides(-1)>&#10094;</a> <a class=carousel-simple-next data-ng-click=plusSlides(1)>&#10095;</a> </div>"
  );

}]);
