/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbCarousel')//

/**
 * 
 * Manage an slider
 */
.controller('AmWbCarouselCtrl', function($scope, $timeout, $interval) {
	var wbModel = $scope.wbModel;
	var timestamp = +new Date();
	var currentIndex = 0;

	/**
	 * Show slide with idx index
	 * 
	 */
	function showSlide(idx) {
		if (idx === undefined) {
			return;
		}
		if (idx >= wbModel.slides.length) {
			idx = 0;
		}
		if (idx < 0) {
			idx = wbModel.slides.length - 1;
		}
		currentIndex = idx;
		$scope.currentIndex = idx;
		$scope.slides = wbModel.slides;
		$scope.slide = wbModel.slides[currentIndex];
	}

	/**
	 * Show next slied
	 */
	function showNext() {
		showSlide(currentIndex + 1);
	}

	/**
	 * Show prev. slied
	 */
	function showPrev() {
		showSlide(currentIndex - 1);
	}

	/**
	 * Skip n slied
	 */
	function plusSlides(n) {
		showSlide(currentIndex + n);
	}

	/**
	 * Push new empty slide
	 */
	function addSlide() {
		wbModel.slides.push({
			image : 'http://media.istockphoto.com/photos/plant-growing-picture-id510222832',
			caption : '<h1>Sample Caption</h1>'
		});
	}
	
	/**
	 * Remove slide in specified index
	 */
	function removeSlide(index) {
		wbModel.slides.splice(index, 1);
		showSlide(index - 1 < 0 ? 0 : index - 1);
	}

	// Slied function in scope
	$scope.plusSlides = plusSlides;
	$scope.showSlide = showSlide;
	$scope.add = addSlide;
	$scope.remove = removeSlide;

	showSlide(0);

	$scope.extraActions = [ {
		title : 'Add slide',
		icon : 'add',
		action : addSlide
	}, {
		title : 'Remove slide',
		icon : 'remove',
		action : function(){
			removeSlide(currentIndex)
		}
	} ];

	$scope.$watch('wbModel.slides', function() {
		showSlide(0);
	});

	// TODO: check if is required in current settins
	var autoplayPromise = $interval(function() {
		if (wbModel.autoplay) {
			showNext();
		}
	}, wbModel.autoplayTime);

	$scope.$watch('wbModel.autoplayTime', function() {
		$interval.cancel(autoplayPromise);
		autoplayPromise = $interval(function() {
			if (wbModel.autoplay) {
				showNext();
			}
		}, wbModel.autoplayTime);
	});
});