/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amWbCarousel')
/**
 * load widgets
 */
.run(function($widget) {
	var slides = [{
        title : 'slide 1',
        value : 0,
        image : 'http://media.istockphoto.com/photos/starry-night-picture-id519760984',
        caption : '<h1>Caption Text for Slide 01</h1>',
        style : {}
    }, {
        title : 'slide 2',
        value : 1,
        image : 'http://media.istockphoto.com/photos/autumn-tree-and-sun-during-sunset-fall-in-park-picture-id520757917',
        caption : '<h1>Caption Text for Slide 02</h1>',
        style : {}
    }, {
        title : 'slide 3',
        value : 2,
        image : 'http://media.istockphoto.com/photos/plant-growing-picture-id510222832',
        caption : '<h1>Caption Text for Slide 03</h1>',
        style : {}
    }];
	
	$widget.newWidget({
	    type: 'AmWbCarouselWidget',
	    templateUrl : 'views/am-wb-carousel-widgets/simple.html',
	    label : 'Carousel',
	    description : 'List of slides as carousel.',
	    icon : 'wb-widget-carousel',
	    help : 'https://gitlab.com/weburger/am-wb-carousel/wikis/home',
	    controller: 'AmWbCarouselCtrl',
	    setting:['carousel-setting'],
	    data:{
	    	name: 'Slider',
	    	slides: slides
	    }
	});

	$widget.newWidget({
		type: 'AmWbCarouselNavigator',
		templateUrl : 'views/am-wb-carousel-widgets/navigator.html',
		label : 'Carousel navigator',
		description : 'List of slides as carousel with a navigator on top.',
		icon : 'wb-widget-carousel',
		help : 'https://gitlab.com/weburger/am-wb-carousel/wikis/home',
		controller: 'AmWbCarouselCtrl',
		setting:[
			'carousel-setting'
		],
		data:{
			name: 'Widget slider',
			slides: slides
		}
	});
});
