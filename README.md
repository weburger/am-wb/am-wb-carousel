# Angular Material Weburger Carousel

A widget to show one or more images as slides cycling as carousel. This is a simple carousel which does not use other libraries to implement carousel. It has dependency to angular-material-weburger.

## Development

We use these tools:

- nodejs
- grunt-cli
- bower

## Build

To build product from source you can run following commands respectively:

	nmp install
	bower install
	grunt build
	
## Rund demo

To run a demo type following commands respectively:

	npm install
	bower install
	grunt demo
	
## Use as library

This project is accessable by bower. To use this by bower run following command:

	bower install am-wb-carousel